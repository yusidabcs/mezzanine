<?php
namespace App;
use Backpack\NewsCRUD\app\Models\Article as ArticleCRUD;
use App\CreatedByTrait;
class Article extends ArticleCRUD
{
    use CreatedByTrait;

    protected $table = 'articles';
    protected $primaryKey = 'id';
    public $timestamps = true;
    // protected $guarded = ['id'];
    protected $fillable = ['slug', 'title', 'content', 'image', 'status', 'category_id', 'featured', 'date','user_id'];
    // protected $hidden = [];
    // protected $dates = [];
    protected $casts = [
        'featured'  => 'boolean',
        'date'      => 'date',
    ];
    
    public function shortDescription($limit = 200){
        $html = trim(preg_replace('/\s+/', ' ', $this->content));
        $html = preg_replace('/<a href=\"(.*?)\">(.*?)<\/a>/', "\\2", $html);
        $html = preg_replace('%(.*?)<p>\s*(<img[^<]+?)\s*</p>(.*)%is', '$1$2$3', $html);
        $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
        $html = strip_tags($html);
        return substr($html, 0, strlen($html) > $limit ? $limit : strlen($html));
    }

    public function url(){
        return url('articles/'.$this->slug);
    }
}
