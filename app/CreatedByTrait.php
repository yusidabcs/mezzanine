<?php namespace App;

use Illuminate\Database\Eloquent\Model;

trait CreatedByTrait {

    /**
     * Stores the user id at each create & update.
     */
    public function save(array $options = [])
    {

        if (\Auth::check())
        {
            $this->user_id = \Auth::user()->id;
        }

        parent::save();
    }


    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function author()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
