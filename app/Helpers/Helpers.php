<?php

function get_news($limit = 10,$category = null,$tags = null){
	return App\Article::paginate($limit);
}

function doStuff() {
  static $cache = null;

  if ($cache === null) {
     $cache = '%heavy database stuff or something%';
  }

  // code using $cache
}
function get_members($limit = null){
	static $members = null;
	static $limit_temp = null;

	if($members === null  && $limit_temp != $limit){

		if($limit === null){
			$members = App\User::whereHas('roles',function($q){
				$q->where('name','Member');
			})->get();
		}else{
			$members = App\User::whereHas('roles',function($q){
				$q->where('name','Member');
			})->paginate($limit);
		}
		$limit_temp = $limit;
	}

	return $members;
}

function get_articles($limit = 6){
	$other = App\Article::with('author')->orderBy('created_at')->paginate($limit);
	return $other;
}

function get_categories($limit = 6){
	$other = Backpack\NewsCRUD\app\Models\Category::all();
	return $other;
}

function get_tags($limit = 6){
	$other = Backpack\NewsCRUD\app\Models\Tag::limit($limit)->get();
	return $other;
}
function other_articles($article,$limit = 6){
	$other = App\Article::with('author')->where('id','!=',$article->id)->take($limit)->get();
	return $other;
}

function author_url($member){

	return url('authors/'.$member->id);

}

function photo_profile($member){

	return asset('storage/'.$member->image);

}


function main_menu(){
	static $menus = null;
	if($menus == null){
		$menu = Backpack\MenuCRUD\app\Models\MenuItem::getTree();
		$menus = $menu;	
	}

	return $menus;
	
}

function galleries($limit = null){
	static $galleries = null;
	static $limit_temp = null;


	if($galleries === null  && $limit_temp != $limit){

		if($limit === null){
			$galleries = Bcscoder\GalleryManager\app\Models\Gallery::all();	
		}else{
			$galleries = Bcscoder\GalleryManager\app\Models\Gallery::paginate($limit);	
		}
		$limit_temp = $limit;
	}

	return $galleries;
	

}