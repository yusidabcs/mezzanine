<?php
namespace App\Http\Controllers\Frontend;

/**
*
*/
class ArticleController extends BaseController
{

	public function index(){
		$view = [];
		return $this->theme->scope('article.index', $view)->render();
	}

  public function show($slug){
		$view = [

      'article' => \App\Article::where('slug',$slug)->first(),

    ];

		return $this->theme->scope('article.show', $view)->render();

	}
}
