<?php
namespace App\Http\Controllers\Frontend;

use Backpack\PageManager\app\Models\Page;
use App\User;
/**
*
*/
class AuthorController extends BaseController
{

	public function index(){
		$view = [
		];
		return $this->theme->scope('author.index', $view)->render();
	}

	public function show($id){
		$view = [
			'member' => User::find($id)
		];
		return $this->theme->scope('author.detail', $view)->render();
	}
}
