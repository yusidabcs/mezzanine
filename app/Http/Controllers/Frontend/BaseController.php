<?php
namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Theme;
/**
* 
*/
class BaseController extends Controller
{
	public $theme;
	
	function __construct()
	{
		
		$this->theme = Theme::uses('mezzanine')->layout('default');

		$this->theme->partialComposer('header', function($view)
		{
		});

	}
}