<?php
namespace App\Http\Controllers\Frontend;

use Backpack\PageManager\app\Models\Page;
use App\User;
use App\Mail\ContactUs;
use Illuminate\Support\Facades\Mail;
/**
*
*/
class ContactController extends BaseController
{

	public function index(){
		$view = [
		];
		return $this->theme->scope('contact.index', $view)->render();
	}

	public function store(){
    $mail = Mail::to(config('settings.contact_email'))->send(new ContactUs());

    return redirect()->back()->with('message','Berhasil mengirim pesan');
	}
}
