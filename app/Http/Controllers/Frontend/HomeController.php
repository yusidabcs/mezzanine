<?php
namespace App\Http\Controllers\Frontend;

use Backpack\PageManager\app\Models\Page;
/**
*
*/
class HomeController extends BaseController
{

	public function index(){
		$view = [
		];
		return $this->theme->scope('home.index', $view)->render();
	}

	public function show($id){

		$view = [
			'page' => Page::where('slug',$id)->first()
		];

		if( view()->exists('theme.'.$this->theme->getThemeName()."::views.page.".$id) ){
		    return $this->theme->scope('page.'.$id,$view)->render();
		}

		
		return $this->theme->scope('page.index', $view)->render();
	}
}
