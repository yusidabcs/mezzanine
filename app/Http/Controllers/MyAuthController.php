<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Backpack\Base\app\Http\Controllers\Auth\LoginController;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;

class MyAuthController extends LoginController
{
    /**
     * Show the application login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        $view = property_exists($this, 'loginView')
                    ? $this->loginView : 'auth.authenticate';

        if (view()->exists($view)) {
            return view($view);
        }

        $this->data['title'] = trans('backpack::base.login'); // set the page title

        return view('auth.login', $this->data);
    }

    public function login(Request $request)
    {
      $this->validateLogin($request);

      // If the class is using the ThrottlesLogins trait, we can automatically throttle
      // the login attempts for this application. We'll key this by the username and
      // the IP address of the client making these requests into this application.
      if ($this->hasTooManyLoginAttempts($request)) {
          $this->fireLockoutEvent($request);

          return $this->sendLockoutResponse($request);
      }

      $credentials = $this->credentials($request);

      if ($this->guard()->attempt($credentials, $request->has('remember'))) {
        if(!Auth::user()->hasRole('Administrator') && !Auth::user()->hasRole('Moderator')){
            Auth::logout();
            return redirect()->back()->withErrors([
                'email' => 'Kamu tidak memiliki akses ke halaman ini.'
                ]);
        }
        return $this->sendLoginResponse($request);

      }

      // If the login attempt was unsuccessful we will increment the number of attempts
      // to login and redirect the user back to the login form. Of course, when this
      // user surpasses their maximum number of attempts they will get locked out.
      $this->incrementLoginAttempts($request);

      return $this->sendFailedLoginResponse($request);
    }

}
