<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::resource('articles','Frontend\ArticleController');
Route::resource('authors','Frontend\AuthorController');
Route::resource('contact','Frontend\ContactController');


Route::resource('','Frontend\HomeController');
Route::get('/{id}','Frontend\HomeController@show');


// Authentication Routes...

Route::group(['prefix' => config('backpack.base.route_prefix')], function () {

    $this->get('login', 'MyAuthController@showLoginForm');
	$this->post('login', 'MyAuthController@login');
	$this->get('logout', 'MyAuthController@logout');

	// Registration Routes...
	$this->get('register', 'MyAuthController@showRegistrationForm');
	$this->post('register', 'MyAuthController@register');

	// Password Reset Routes...
	$this->get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
	$this->post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
	$this->post('password/reset', 'Auth\PasswordController@reset');


});

Route::group(['prefix' => 'admin', 'middleware' => ['web', 'admin'], 'namespace' => 'Admin'], function () {
    	\CRUD::resource('article', 'ArticleCrudController');
      \CRUD::resource('user', 'UserCrudController');
});

Route::get('register',function(){
  App\User::create([
    'email' => 'agusyusida@gmail.com',
    'password' => bcrypt('secret'),
  ]);
});
