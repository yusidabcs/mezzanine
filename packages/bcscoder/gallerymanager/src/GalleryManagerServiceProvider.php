<?php 
namespace Bcscoder\GalleryManager;
use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;
use Route;

class GalleryManagerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // publish migrations
        $this->publishes([__DIR__.'/database/migrations' => database_path('migrations')], 'migrations');
    }

    public function setupRoutes(Router $router)
    {
        $router->group(['namespace' => 'Bcscoder\GalleryManager\app\Http\Controllers'], function ($router) {
            // Admin Interface Routes
            Route::group(['middleware' => ['web', 'admin'], 'prefix' => 'admin'], function () {
                Route::resource('gallery', 'Admin\GalleryCrudController');
            });
        });
    }


    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
     	$this->app->bind('gallerymanager', function ($app) {
            return new GalleryManager($app);
        });

        // register its dependencies
     	$this->setupRoutes($this->app->router);
    }
}
