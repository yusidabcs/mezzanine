<?php

namespace Bcscoder\GalleryManager\app\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

use Bcscoder\GalleryManager\app\Http\Requests\GalleryRequest as StoreRequest;
use Bcscoder\GalleryManager\app\Http\Requests\GalleryRequest as UpdateRequest;

class GalleryCrudController extends CrudController
{

    public function __construct($template_name = false)
    {
        parent::__construct();

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel("Bcscoder\GalleryManager\app\Models\Gallery");
        $this->crud->setRoute('admin/gallery');
        $this->crud->setEntityNameStrings('gallery', 'galleries');

        /*
        |--------------------------------------------------------------------------
        | COLUMNS
        |--------------------------------------------------------------------------
        */

        $this->crud->addColumn([ // image
                'label' => "Image",
                'type' => "model_function",
                'function_name' => 'getImage',
            ]);

        /*
        |--------------------------------------------------------------------------
        | FIELDS
        |--------------------------------------------------------------------------
        */

        // In PageManager,
        // - default fields, that all templates are using, are set using $this->addDefaultPageFields();
        // - template-specific fields are set per-template, in the PageTemplates trait;


        /*
        |--------------------------------------------------------------------------
        | BUTTONS
        |--------------------------------------------------------------------------
        */
        
    }

    // -----------------------------------------------
    // Overwrites of CrudController
    // -----------------------------------------------

    // Overwrites the CrudController create() method to add template usage.
    public function create($template = false)
    {
        $this->addDefaultPageFields($template);

        return parent::create();
    }

    // Overwrites the CrudController store() method to add template usage.
    public function store(StoreRequest $request)
    {
        $this->addDefaultPageFields(\Request::input('template'));
        return parent::storeCrud();
    }

    // Overwrites the CrudController edit() method to add template usage.
    public function edit($id, $template = false)
    {
        $this->addDefaultPageFields($template);
        return parent::edit($id);
    }

    // Overwrites the CrudController update() method to add template usage.
    public function update(UpdateRequest $request)
    {
        $this->addDefaultPageFields();
        return parent::updateCrud();
    }

    // -----------------------------------------------
    // Methods that are particular to the PageManager.
    // -----------------------------------------------

    /**
     * Populate the create/update forms with basic fields, that all pages need.
     *
     * @param string $template The name of the template that should be used in the current form.
     */
    public function addDefaultPageFields($template = false)
    {
        
        $this->crud->addField([
                                'name' => 'image',
                                'label' => 'Image',
                                'type' => 'browse',
                                'wrapperAttributes' => [
                                    'class' => 'form-group col-md-12',
                                ],
                            ]);

        $this->crud->addField([
                                'name' => 'url',
                                'label' => 'Url',
                                'type' => 'text',
                                'wrapperAttributes' => [
                                    'class' => 'form-group col-md-12',
                                ],
                            ]);

        $this->crud->addField([
                                'name' => 'description',
                                'label' => 'Description',
                                'type' => 'wysiwyg',
                                'wrapperAttributes' => [
                                    'class' => 'form-group col-md-12',
                                ],
                            ]);

        $this->crud->addField([
                                'name' => 'status',
                                'label' => 'Main Gallery?',
                                'type' => 'checkbox',
                            ]);

       
    }

    /**
     * Add the fields defined for a specific template.
     *
     * @param  string $template_name The name of the template that should be used in the current form.
     */
    public function useTemplate($template_name = false)
    {
        $templates = $this->getTemplates();

        // set the default template
        if ($template_name == false) {
            $template_name = $templates[0]->name;
        }

        // actually use the template
        if ($template_name) {
            $this->{$template_name}();
        }
    }

    /**
     * Get all defined templates.
     */
    public function getTemplates()
    {
        $templates_array = [];

        $templates_trait = new \ReflectionClass('App\PageTemplates');
        $templates = $templates_trait->getMethods();

        if (! count($templates)) {
            abort('403', 'No templates have been found.');
        }

        return $templates;
    }
}
