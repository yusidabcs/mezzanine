<?php 
namespace Bcscoder\TeamManager;
use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;
use Route;

class TeamManagerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // publish migrations
        $this->publishes([__DIR__.'/database/migrations' => database_path('migrations')], 'migrations');
    }

    public function setupRoutes(Router $router)
    {
        $router->group(['namespace' => 'Bcscoder\TeamManager\app\Http\Controllers'], function ($router) {
            // Admin Interface Routes
            Route::group(['middleware' => ['web', 'admin'], 'prefix' => 'admin'], function () {
                Route::resource('teams', 'Admin\TeamCrudController');
            });
        });
    }


    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
     	$this->app->bind('teammanager', function ($app) {
            return new TeamManager($app);
        });

        // register its dependencies
     	$this->setupRoutes($this->app->router);
    }
}
