<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Full Slider - Start Bootstrap Template</title>

    <!-- Bootstrap Core CSS -->
    
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Raleway:100,300,400">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:100,300,400">
    <link rel="stylesheet" type="text/css" href="{{ Theme::asset()->url('css/bootstrap.css') }}">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{ Theme::asset()->url('css/bootstrap-social.css') }}">

        <link rel="stylesheet" type="text/css" href="{{ Theme::asset()->url('css/magnific-popup.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ Theme::asset()->url('css/custom.css') }}">
<!-- Magnific Popup core CSS file -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    {!! Theme::asset()->styles() !!}
    {!! Theme::asset()->scripts() !!}

</head>

<body>

    <!-- Navigation -->
    {!! Theme::partial('header') !!}

    {!! Theme::content() !!}

    {!! Theme::partial('footer') !!}

    <!-- jQuery -->
    <script   src="http://code.jquery.com/jquery-2.2.4.min.js"   integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="   crossorigin="anonymous"></script>
        <script type="text/javascript" src="{{ Theme::asset()->url('js/bootstrap.min.js') }}"></script>

        <script type="text/javascript" src="{{ Theme::asset()->url('js/jcorousel.js') }}"></script>

<script src="https://unpkg.com/masonry-layout@4.1/dist/masonry.pkgd.min.js"></script>
<script type="text/javascript" src="{{ Theme::asset()->url('js/jquery.magnific-popup.min.js') }}"></script>
    <!-- Script to Activate the Carousel -->
    <script>
    $(document).ready(function () {
        $(".carousel").carousel();

        $(window).resize(function () {
            $(".carousel").carousel();
        });

        $('.grid').masonry({
          itemSelector: '.grid-item',
        });

        $('.image-link').magnificPopup({type:'image'});
    });

    (function($) {
    $(function() {
        var jcarousel = $('.jcarousel');

        jcarousel
            .on('jcarousel:reload jcarousel:create', function () {
                var carousel = $(this),
                    width = carousel.innerWidth();

                if (width >= 600) {
                    width = width / 4;
                } else if (width >= 350) {
                    width = width / 2;
                }

                carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
                carousel.jcarousel('items').css('height', Math.ceil(width) + 'px');
            })
            .jcarousel({
                wrap: 'circular'
            });

        $('.jcarousel-control-prev')
            .jcarouselControl({
                target: '-=1'
            });

        $('.jcarousel-control-next')
            .jcarouselControl({
                target: '+=1'
            });

        $('.jcarousel-pagination')
            .on('jcarouselpagination:active', 'a', function() {
                $(this).addClass('active');
            })
            .on('jcarouselpagination:inactive', 'a', function() {
                $(this).removeClass('active');
            })
            .on('click', function(e) {
                e.preventDefault();
            })
            .jcarouselPagination({
                perPage: 1,
                item: function(page) {
                    return '<a href="#' + page + '">' + page + '</a>';
                }
            });
    });
})(jQuery);
    </script>

</body>

</html>
