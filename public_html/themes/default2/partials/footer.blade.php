<div class="container">
  <footer>
    <div class="row">
      <div class="col-lg-8">

        <ul class="list-unstyled float-left">
          @foreach(main_menu() as $m)

          @if(count($m->children) > 0)
          
          @else
          <li>
            <a href="{{$m->url()}}">{{$m->name}}</a>
          </li>
          @endif
          @endforeach
        </ul>
        <div class="clearfix"></div>
        <p>© 2016 Mezzani Club. Some Rights Reserved.</p>
        <p></p>
      </div>
      <div class="col-lg-4">
        <div class="clearfix"></div>

        <ul class="pull-right list-unstyled">
          <li>
            <a class="btn btn-social-icon btn-facebook" href="{{ config('settings.facebook') }}">
              <span class="fa fa-facebook"></span>
            </a>
          </li>
          <li>
            <a class="btn btn-social-icon btn-twitter" href="{{ config('settings.twitter') }}">
              <span class="fa fa-twitter"></span>
            </a>
          </li>

          <li>
            <a class="btn btn-social-icon btn-instagram" href="{{ config('settings.instagram') }}">
              <span class="fa fa-instagram"></span>
            </a>
          </li>

          <li>
            <a class="btn btn-social-icon btn-youtube" href="{{ config('settings.youtube') }}">
              <span class="fa fa-youtube"></span>
            </a>
          </li>
        </ul>

      </div>
    </div>

  </footer>
</div>
