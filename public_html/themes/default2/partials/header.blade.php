<nav class="navbar navbar-default">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{ url('')  }}">Mezzani Club</a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        @foreach(main_menu() as $m)

        @if(count($m->children) > 0)
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{$m->name}} <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            @foreach($m->children as $second_menu)
            <li><a href="{{ $second_menu->url() }}">{{$second_menu->name}}</a></li>
            @endforeach
          </ul>
        </li>
        @else
        <li>
          <a href="{{$m->url()}}">{{$m->name}}</a>
        </li>
        @endif
        @endforeach

      </ul>

    </div>
  </div>
</nav>
