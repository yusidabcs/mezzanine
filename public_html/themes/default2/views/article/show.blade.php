
<section class="detail">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <ol class="breadcrumb">
          <li><a href="{{ url('')  }}">Beranda</a></li>
          <li><a href="#">{{ $article->title }}</a></li>
        </ol>
        <article>
          <h1>{{ $article->title }}</h1>
          <div class="row separator">
            <div class="col-lg-6">
              <p class="legend">oleh: <a href="#">{{ $article->author->name }}</a></p>
            </div>
            <div class="col-lg-6">
              <p class="legend">Dibuat: {{ $article->created_at }}</p>
            </div>
          </div>
          <div class="body">
          <img src="{{ $article->image }}" class="img img-responsive"  />
          {!! $article->content !!}
        </article>
        <hr>
        <div class="fb-comments" data-href="{{ url()->current() }}" data-numposts="5"></div>
      </div>

      <div class="col-md-4">
            <div class="list-group">
              <a href="#" class="list-group-item">
                <h3>Berita Lainnya</h3>
              </a>

              @foreach(other_articles($article) as $other_article)
              <a href="#" class="list-group-item">
                <h5 class="list-group-item-heading">{{ $other_article->title }}</h5>
                <p class="list-group-item-text">{{ $other_article->shortDescription() }}</p>
              </a>
              @endforeach

            </div>

      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">

      </div>
    </div>
  </section>

  <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.8&appId=1651021911881015";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
