
<section class="detail">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
          <h1 class="text-center">Tentang Mezzani Club</h1>
          <p class="lead text-center">Mereka adalah sarjana-sarjana terbaik dari berbagai penjuru tanah air. Mereka terpanggil untuk menjadi Pengajar Muda: ikut membantu mencerdaskan kehidupan bangsa melalui langkah nyata di bidang pendidikan. Menjadi Pengajar Muda bukanlah pengorbanan. Ini adalah kesempatan sekaligus kehormatan besar untuk mengenal bangsa Indonesia secara langsung dan utuh. Selama setahun di daerah penempatan, mereka mengajar, berinteraksi dan membagi inspirasi.</p>
          <hr style="margin: 70px auto">
      </div>
    </div>
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="row">
          <div class="col-md-6">
            <h2>{{ $member->name }}</h2>
            {!! $member->about !!}
            <div class="row">
              <div class="col-md-6">
                <h4>Connect</h4>
                <ul style="list-style:none;padding-left:5px;">
                  <li style="margin-top:16px;">
                    <a class="btn btn-social-icon btn-facebook btn-xs">
                      <span class="fa fa-facebook"></span>
                    </a>
                  </li>
                  <li style="margin-top:16px;">
                    <a class="btn btn-social-icon btn-twitter btn-xs">
                      <span class="fa fa-twitter"></span>
                    </a>
                  </li>

                  <li style="margin-top:16px;">
                    <a class="btn btn-social-icon btn-instagram btn-xs">
                      <span class="fa fa-instagram"></span>
                    </a>
                  </li>

                  <li style="margin-top:16px;">
                    <a class="btn btn-social-icon btn-linkedin btn-xs">
                      <span class="fa fa-linkedin"></span>
                    </a>
                  </li>
                </ul>
              </div>
              <div class="col-md-6">
                <h4>Blog</h4>

                <ul  style="padding-left:16px">
                  @foreach($member->articles as $article)
                  <li><a href="{{ $article->url() }}"> {{ $article->title }}  </a></li>
                  @endforeach
                </ul>
              </div>
            </div>

          </div>
          <div class="col-md-6">
            <img src="{{ photo_profile($member) }}" class="img img-responsive img-rounded"/>
          </div>
      </div>
    </div>

</section>
<section class="detail">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3 text-center">
          <hr>
          <a class="lead" href="{{ url('authors') }}">Lihat Tim yang Lain</a>
      </div>
    </div>
  </div>
</section>
