
<section class="detail">
  <div class="container">
    <div class="row">
      <div class="col-md-12">

      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <h1 >{{ config('settings.contact_title') }}</h1>

        <hr style="margin:60px auto">
        {{Form::open(['url' => url('contact') , 'method' => 'post'])}}
          <div class="form-group">
            <label for="exampleInputEmail1">Nama</label>
            <input type="text" name="name" class="form-control" id="exampleInputEmail1" placeholder="Nama kamu" required="">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Email</label>
            <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Email" required="">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Pesan</label>
            <textarea class="form-control" name="message" required=""></textarea>
          </div>
          <button type="submit" class="btn btn-default">Submit</button>
        </form>

      </div>

      <div class="col-md-6">
        <address>
          <strong>{{ config('settings.address') }}</strong><br>
          <abbr title="Phone">P:</abbr> {{ config('settings.phone') }}<br>
          <abbr title="Fax">F:</abbr> {{ config('settings.fax') }}<br>
        </address>
        

        <div style="width: 100%">

        <iframe width="100%" height="600" src="http://www.maps.ie/create-google-map/map.php?width=100%&amp;height=600&amp;hl=en&amp;coord={{json_decode(config('settings.location'))->lat}},{{json_decode(config('settings.location'))->lng}}&amp;q=1%20{{ config('settings.address') }}+({{ config('settings.website_title') }})&amp;ie=UTF8&amp;t=&amp;z=12&amp;iwloc=A&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0">
          
        </iframe></div><br />

      </div>
    </div>

</section>
