<!-- Full Page Image Background Carousel Header -->
<header id="myCarousel" class="carousel slide">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
  </ol>

  <!-- Wrapper for Slides -->
  <div class="carousel-inner">

    @foreach(get_main_gallery() as  $key => $g)
    <div class="item  {{ $key == 0 ? 'active' : '' }}" >
      <!-- Set the first background image using inline CSS below. -->
      <div class="fill" style="background-image:url('{{ url($g->image)  }}');"></div>
      <div class="carousel-caption">
        <h2>{!! $g->description !!}</h2>
      </div>
    </div>
    @endforeach

  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="icon-prev"></span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="icon-next"></span>
  </a>

</header>
<section class="bg-light-gray">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h2 class="section-heading">Our Amazing Team</h2>
        <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
      </div>
    </div>
    <div class="row">

      @foreach(get_members(4) as $member)
      <div class="col-sm-3 col-xs-6">
        <div class="team-member">
          <div class="row">
            <div class="col-md-10 col-md-offset-1">
              <a href="{{ author_url($member) }}" ><img src="{{ $member->imageURL() }}" class="img-responsive img-circle" alt=""></a>
            </div>
          </div>

          <h5 class="text-center">{{ $member->name }}</h5>
          <ul class="list-inline social-buttons">
            <li>
              <a href="{{ $member->facebook }}" class="text-facebook">
                <span class="fa-stack fa-lg">
                  <i class="fa fa-circle fa-stack-2x"></i>
                  <i class="fa fa-facebook fa-stack-1x fa-inverse text-info"></i>
                </span>
              </a>
            </li>
            <li>
              <a href="{{ $member->twitter }}" class="text-twitter">
                <span class="fa-stack fa-lg">
                  <i class="fa fa-circle fa-stack-2x"></i>
                  <i class="fa fa-twitter fa-stack-1x fa-inverse text-info"></i>
                </span>
              </a>
            </li>
            <li>
              <a href="{{ $member->instagram }}" class="text-instagram">
                <span class="fa-stack fa-lg">
                  <i class="fa fa-circle fa-stack-2x"></i>
                  <i class="fa fa-instagram fa-stack-1x fa-inverse text-info"></i>
                </span>
              </a>
            </li>
          </ul>
        </div>
      </div>
      @endforeach
    </div>
  </div>
</section>
<section style="padding-top: 0;padding-bottom: 0;">
  <div class="container-fluid">
    <div class="jcarousel-wrapper">
      <div class="jcarousel">
        <ul>
          @foreach(get_gallery() as  $key => $g)
          <li>
          <a href="{{ $g->image }}" class="image-link" title="{{ $g->description }}">
            <div class="fill" style="background-image:url('{{ url($g->image) }}');background-position: center center; "></div></a>
          </li>
          @endforeach
        </ul>
      </div>
      <a class="jcarousel-control-prev">&lsaquo;</a>
      <a class="jcarousel-control-next">&rsaquo;</a>
    </div>
  </div>
</section>
<section class="bg-light-gray">
  <div class="container">

    
    <h2 class="section-heading text-center">Berita Terbaru</h2>
    <h3 class="section-subheading text-muted text-center">Lorem ipsum dolor sit amet consectetur.</h3>


    <div class="row">
      @foreach(get_news(3) as $news)
      <div class="col-lg-4">
        <div class="well" >
          <h4>{{ $news->title }}</h4>
          <div class="row separator">
            <div class="col-lg-6">
              <p class="legend">oleh: <a href="">{{ $news->author->name }}</a></p>
            </div>
            <div class="col-lg-6">
              <p class="legend">{{ $news->created_at }}</p>
            </div>
          </div>
          <p>
            {{$news->shortDescription()}}
          </p>
          <center><a href="{{ $news->url() }}" class="btn btn-sm btn-primary">Read More</a></center>
        </div>
      </div>
      @endforeach
    </div>
  </div>
</section>
