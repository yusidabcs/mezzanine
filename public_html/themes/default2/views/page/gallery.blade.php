<section class="detail">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <article>
          <h1 class="text-center">{{ $page->title }} 2</h1>
          <hr>
          <div class="body">
            {!! $page->content !!}

          </div>
        </article>

        <div class="grid">
          @foreach(galleries(12) as $gallery)
          <div class="grid-item">
            <a href="{{ $gallery->image }}" class="image-link" title="{{ $gallery->description }}"><img src="{{ $gallery->image }}" alt="" class="img-responsive"></a>
          </div>
          @endforeach
        </div>
        <div class="clearfix"></div>




      </div>

    </div>

  </div>

</section>
