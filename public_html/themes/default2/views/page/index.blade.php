
<section class="detail">
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <article>
          <h1 class="text-center">{{ $page->title }}</h1>
          <hr>
          <div class="body">
          {!! $page->content !!}

          </div>
        </article>
      </div>

    </div>

</section>
