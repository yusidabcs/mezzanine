<!DOCTYPE html>
<html lang="en" class="no-js">
  <head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
    <title>Insomnia Multi-Purpose - Beautiful and Modern HTML 5 / CSS 3 Corporate Template</title>
    <meta content="Insomnia - responsive and retina ready template" name="description">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/>
    <link href="{{ Theme::asset()->url('images/favicon.png') }}" rel="shortcut icon"/>
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/images/apple-touch-icon-144x144-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/images/apple-touch-icon-114x114-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/images/apple-touch-icon-72x72-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" href="assets/images/apple-touch-icon-precomposed.png" />
    <!-- JS FILES -->
    <script type="text/javascript" src="{{ Theme::asset()->url('js/jquery-1.12.0.min.js') }}"></script>
    <script type="text/javascript" src="{{ Theme::asset()->url('js/jquery-migrate-1.2.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ Theme::asset()->url('js/modernizr.custom.js') }}"></script>
    <script type="text/javascript" src="{{ Theme::asset()->url('js/isotope.pkgd.min.js') }}"></script>
    <!-- CSS FILES -->
    <link href="{{ Theme::asset()->url('rs-plugin/css/settings.css') }}" media="screen" rel="stylesheet" type="text/css">
    <link href="{{ Theme::asset()->url('rs-plugin/css/layers.css') }}" media="screen" rel="stylesheet" type="text/css">
    <link href="{{ Theme::asset()->url('rs-plugin/css/navigation.css') }}" media="screen" rel="stylesheet" type="text/css">
    <link href="{{ Theme::asset()->url('css/style-wide.css') }}" media="screen" rel="stylesheet" type="text/css">
    <link href="{{ Theme::asset()->url('css/responsive.css') }}" media="screen" rel="stylesheet" type="text/css">
    <link href="{{ Theme::asset()->url('css/prettyphoto.css') }}" media="screen" rel="stylesheet" type="text/css">
    <link href="{{ Theme::asset()->url('css/layouts/default.css') }}" media="screen" rel="stylesheet" type="text/css">
  </head>
  <body>
    <div class="wrapper">
      
      <!-- Navigation -->
    {!! Theme::partial('header') !!}


    {!! Theme::content() !!}

    {!! Theme::partial('footer') !!}
      
    <script type="text/javascript" src="{{ Theme::asset()->url('rs-plugin/js/jquery.themepunch.tools.min.js') }}"></script>
    <script type="text/javascript" src="{{ Theme::asset()->url('rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>
    <script type="text/javascript" src="{{ Theme::asset()->url('rs-plugin/js/extensions/revolution.extension.actions.min.js') }}"></script>
    <script type="text/javascript" src="{{ Theme::asset()->url('rs-plugin/js/extensions/revolution.extension.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ Theme::asset()->url('rs-plugin/js/extensions/revolution.extension.kenburn.min.js') }}"></script>
    <script type="text/javascript" src="{{ Theme::asset()->url('rs-plugin/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
    <script type="text/javascript" src="{{ Theme::asset()->url('rs-plugin/js/extensions/revolution.extension.migration.min.js') }}"></script>
    <script type="text/javascript" src="{{ Theme::asset()->url('rs-plugin/js/extensions/revolution.extension.navigation.min.js') }}"></script>
    <script type="text/javascript" src="{{ Theme::asset()->url('rs-plugin/js/extensions/revolution.extension.parallax.min.js') }}"></script>
    <script type="text/javascript" src="{{ Theme::asset()->url('rs-plugin/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
    <script type="text/javascript" src="{{ Theme::asset()->url('rs-plugin/js/extensions/revolution.extension.video.min.js') }}"></script>
    <script type="text/javascript" src="{{ Theme::asset()->url('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ Theme::asset()->url('js/retina.min.js') }}"></script>
    <script type="text/javascript" src="{{ Theme::asset()->url('js/jquery.prettyphoto.js') }}"></script>
    <script type="text/javascript" src="{{ Theme::asset()->url('js/waypoints.min.js') }}"></script>
    <script type="text/javascript" src="{{ Theme::asset()->url('js/sticky.js') }}"></script>
    <script type="text/javascript" src="{{ Theme::asset()->url('js/jquery.inview.js') }}"></script>
    <script type="text/javascript" src="{{ Theme::asset()->url('js/jquery.parallax-1.1.3.js') }}"></script>
    <script type="text/javascript" src="{{ Theme::asset()->url('js/jcarousel.responsive.js') }}"></script>
    <script type="text/javascript" src="{{ Theme::asset()->url('js/jquery.jcarousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ Theme::asset()->url('js/main.js') }}"></script>
  </body>
</html>