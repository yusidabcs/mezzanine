
<div class="footer">
  <div class="container">
    <div class="row">
      <div class="col-lg-3 col-md-3 col-sm-3">
        <div class="footer-widget">
          <div class="footer-title">About Us</div>
          <div class="text_footer">
            {{ config('settings.about') }}
          </div>
          <ul class="soc-footer">
            <li><a href="#"><i class="ti-facebook"></i></a></li>
            <li><a href="#"><i class="ti-twitter"></i></a></li>
            <li><a href="#"><i class="ti-instagram"></i></a></li>
          </ul>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-4">
        <div class="footer-widget">
          <div class="footer-title">Hubungi Kami</div>
          <ul class="contact-footer contact-composer">
            <li><i class="pe-7s-home"></i> <span>Adress: {{ config('settings.address') }}</span></li>
            <li><i class="pe-7s-call"></i> <span>Phone: {{ config('settings.phone') }}</span></li>
            <li><i class="pe-7s-mail"></i> <span>E-mail: {{ config('settings.email') }}</span></li>
          </ul>
        </div>
      </div>

    </div>
  </div>
  <div class="footer-bottom">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-ms-12 pull-left">
          <div class="copyright">Copyright © 2016 Mezzanine Club. </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-ms-12 pull-right">
          <div class="foot_menu">
            <div class="menu-footer-menu-container">
             <ul>
               @foreach(main_menu() as $m)

          @if(count($m->children) > 0)
          
          @else
          <li>
            <a href="{{$m->url()}}">{{$m->name}}</a>
          </li>
          @endif
          @endforeach
             </ul>
            </div>            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>