<header>
        <div class="page_head classic-vers">
            <div id="nav-container" class="nav-container" style="height: auto;">
                <nav>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 pull-left">
                                <div class="logo">
                                    <a href="{{ url('') }}"><span class="pull-left logo-text classic-vers">Mezzanine Club</span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-6 col-xs-6">
                              <div class="menu menu-wrapper classic-vers">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"></button>
                              <div class="navbar-collapse collapse">
                                <div class="menu-main-menu-container">
                                  <ul>
                                    @foreach(main_menu() as $m)

                                    @if(count($m->children) > 0)
                                    <li class="dropdown">
                                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{$m->name}} <span class="caret"></span></a>
                                      <ul class="dropdown-menu" role="menu">
                                        @foreach($m->children as $second_menu)
                                        <li><a href="{{ $second_menu->url() }}">{{$second_menu->name}}</a></li>
                                        @endforeach
                                      </ul>
                                    </li>
                                    @else
                                    <li>
                                      <a href="{{$m->url()}}">{{$m->name}}</a>
                                    </li>
                                    @endif
                                    @endforeach
                                    
                                </ul>
                              </div>
                            </div>
                          </div>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
      </header>