<!-- Full Page Image Background Carousel Header -->
<header id="myCarousel" class="carousel slide">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
  </ol>

  <!-- Wrapper for Slides -->
  <div class="carousel-inner">
    <div class="item active">
      <!-- Set the first background image using inline CSS below. -->
      <div class="fill" style="background-image:url('https://scontent-sin6-1.xx.fbcdn.net/v/t1.0-9/12809553_1576055586054022_3959621584942200170_n.jpg?oh=c6ae01e95cb76a03a9c80c0e265747a2&oe=58A15AAA');"></div>
      <div class="carousel-caption">
        <h2>Caption 1</h2>
      </div>
    </div>
    <div class="item">
      <!-- Set the second background image using inline CSS below. -->
      <div class="fill" style="background-image:url('https://scontent-sin6-1.xx.fbcdn.net/v/t1.0-9/12115818_1611687809157466_1578459628441578457_n.jpg?oh=ebc07c8561b72aecdfd9b5ec8d32edc8&oe=58A1C414');"></div>
      <div class="carousel-caption">
        <h2>Caption 2</h2>
      </div>
    </div>
    
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="icon-prev"></span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="icon-next"></span>
  </a>

</header>
