<div class="page-in">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <div class="page-in-name">Berita Terbaru</div>
              <div class="page-in-bread">You are here: <a href="#">Home</a> \ Berita Terbaru</div>              
            </div>            
          </div>
        </div>
      </div>
      <div class="container marg50">
        <div class="row">
          <div class="col-lg-9">

            @foreach(get_articles(6) as $article)
            <div class="classic-blog">
              @if($article->image)
              <div class="cl-blog-img"><img src="{{ $article->image }}" alt=""></div>
              @endif
              <div class="cl-blog-naz">
                <div class="cl-blog-type"><i class="pe-7s-pen"></i></div>
                <div class="cl-blog-name"><a href="#">{{ $article->title }}</a></div>
                <div class="cl-blog-detail">{{ date('d M Y, h:i', strtotime($article->created_at)) }}, by <a href="#">{{ $article->author->name }}</a>, in <a href="#">Envato</a></div>
                
                <div class="cl-blog-text">{{ $article->shortDescription(500) }}</div>
              </div>
              <div class="cl-blog-read"><a href="{{ $article->url() }}">Read More</a></div>
              <div class="cl-blog-line"></div>
            </div>
            @endforeach
            
           
            <div class="row">
              <div class="col-lg-12">
                {{ get_articles(6)->links() }}
              </div>
            </div>
          </div> 
          <div class="col-lg-3">
            <div class="promo-text-blog">Category</div>
            <ul class="blog-category">
              @foreach(get_categories() as $category)
              <li><i class="fa fa-angle-right"></i> <a href="#">{{ $category->name }}</a></li>
              @endforeach
            </ul>
            
            <div class="promo-text-blog">Popular Tag's</div>
            <ul class="tags-blog">
              
              @foreach(get_tags() as $tag)
              <li><a href="#">{{ $tag->name }}</a></li>
              @endforeach
            </ul>
          </div>          
        </div>
      </div>