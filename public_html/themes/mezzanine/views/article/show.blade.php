<div class="page-in">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <div class="page-in-name">{{ $article->title }}</div>
              <div class="page-in-bread">You are here: <a href="#">Home</a> \ Blog</div>              
            </div>            
          </div>
        </div>
      </div>
      <div class="container marg50">
        <div class="row">
          <div class="col-lg-12">
            <div class="classic-blog blog-single">
              @if($article->image)
              <div class="cl-blog-img"><img src="{{ $article->image }}" alt=""></div>
              @endif
              <div class="cl-blog-naz">
                <div class="cl-blog-type"><i class="pe-7s-pen"></i></div>
                <div class="cl-blog-detail">{{ date('d M Y, H:i', strtotime($article->created_at)) }}, by <a href="#">{{ $article->author->name }}</a></div>
                <br>
                <div class="cl-blog-text">
                {!! $article->content !!}
                </div> 
              </div>
            </div>
            <div class="row marg50">
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <div class="tags-blog-single">
                  <ul class="tags-blog">
                    <li><a href="#">sapien</a></li>
                    <li><a href="#">posuere</a></li>
                    <li><a href="#">interdum</a></li>
                    <li><a href="#">lectus</a></li>
                    <li><a href="#">velit</a></li>
                    <li><a href="#">eros</a></li>
                    <li><a href="#">quis</a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="author-bio">
              <div class="img-author"><img src="{{ $article->author->image }}" alt=""></div>
              <div class="name-author">About this author</div>
              <div class="text-author">{!! $article->author->about !!}</div>
            </div>
            
          </div> 
        </div>
      </div>


  <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.8&appId=1651021911881015";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
