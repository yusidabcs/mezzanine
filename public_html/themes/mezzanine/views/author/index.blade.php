<section class="bg-light-gray">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h1 class="text-center">Tentang Mezzani Club</h1>
        <p class="lead text-center">Mereka adalah sarjana-sarjana terbaik dari berbagai penjuru tanah air. Mereka terpanggil untuk menjadi Pengajar Muda: ikut membantu mencerdaskan kehidupan bangsa melalui langkah nyata di bidang pendidikan. Menjadi Pengajar Muda bukanlah pengorbanan. Ini adalah kesempatan sekaligus kehormatan besar untuk mengenal bangsa Indonesia secara langsung dan utuh. Selama setahun di daerah penempatan, mereka mengajar, berinteraksi dan membagi inspirasi.</p>
        <hr style="margin: 70px auto">
      </div>
    </div>
    <div class="row">

      @foreach(get_members(12) as $key => $member)
      <div class="col-sm-3">
        <div class="team-member">
          <div class="row">
            <div class="col-md-10 col-md-offset-1">
              <a href="{{ author_url($member) }}" ><img src="{{ $member->imageURL() }}" class="img-responsive img-circle" alt=""></a>
            </div>
          </div>

          <h4 class="text-center">{{ $member->name }}</h4>
          <ul class="list-inline social-buttons">
            <li>
              <a href="{{ $member->facebook }}" class="text-facebook">
                <span class="fa-stack fa-lg">
                  <i class="fa fa-circle fa-stack-2x"></i>
                  <i class="fa fa-facebook fa-stack-1x fa-inverse text-info"></i>
                </span>
              </a>
            </li>
            <li>
              <a href="{{ $member->twitter }}" class="text-twitter">
                <span class="fa-stack fa-lg">
                  <i class="fa fa-circle fa-stack-2x"></i>
                  <i class="fa fa-twitter fa-stack-1x fa-inverse text-info"></i>
                </span>
              </a>
            </li>
            <li>
              <a href="{{ $member->instagram }}" class="text-instagram">
                <span class="fa-stack fa-lg">
                  <i class="fa fa-circle fa-stack-2x"></i>
                  <i class="fa fa-instagram fa-stack-1x fa-inverse text-info"></i>
                </span>
              </a>
            </li>
          </ul>
        </div>
      </div>

      @if($key + 1 % 4 == 0)
      </div>
      <div class="row">
      @endif
      @endforeach

      <div class="row">
        <div class="col-md-10 col-md-offset-1 text-center">
          <hr>
          {{ get_members(12)->links() }}
        </div>
      </div>

    </div>
  </div>
</section>
