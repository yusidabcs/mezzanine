
<div class="page-in contacts">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="page-in-name">{{ config('settings.contact_title') }}</div>
        <div class="page-in-bread">You are here: <a href="#">Home</a> \ Contacts</div>              
      </div>            
    </div>
  </div>
</div>
<div class="container marg75">
  <div class="row">
    <div class="col-lg-12">
      <div class="dankovteam-shortcode-promo-block">
        <div class="promo-block">
          <div class="promo-text">Where Find Us?</div>
          <div class="promo-paragraph">Attended no do thoughts me on dissuade scarcely</div>
          <div class="center-line"></div>
        </div>
      </div>
    </div>
  </div>
</div> 
<div class="container marg30">
  <div class="row">
    <div class="col-lg-12">
      <iframe width="100%" height="500" src="http://www.maps.ie/create-google-map/map.php?width=100%&amp;height=500&amp;hl=en&amp;coord={{json_decode(config('settings.location'))->lat}},{{json_decode(config('settings.location'))->lng}}&amp;q=1%20{{ config('settings.address') }}+({{ config('settings.website_title') }})&amp;ie=UTF8&amp;t=&amp;z=12&amp;iwloc=A&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
    </div> 
  </div>
</div>  
<div class="container marg75">
  <div class="row">
    <div class="col-lg-8 col-md-8 col-sm-12">
      <div class="promo-block">
        <div class="promo-text">Contact Form</div>
        <div class="center-line"></div>
      </div>
      <div class="marg50">
        <div class="row">
          {{Form::open(['url' => url('contact') , 'method' => 'post','id'=>'contactForm'])}}
            <div class="col-lg-6">
              <p class="text_cont"><input type="text" name="name" placeholder="Name" class="input-cont-textarea" required=""></p>
            </div>
            <div class="col-lg-6">
              <p class="text_cont"><input type="text" name="email" placeholder="E-mail" class="input-cont-textarea" required=""></p>
            </div>
            <div class="col-lg-12">
              <p class="text_cont"><input type="text" name="subject" placeholder="Subject" class="input-cont-textarea"></p>
            </div>
            <div class="col-lg-12">
              <div class="alert alert-danger error" id="nameError"><i class="icon-cancel"></i> Oh snap! Name field can't stay empty.</div>
              <div class="alert alert-danger error" id="emailError"><i class="icon-cancel"></i> Oh snap! There was a mistake when writing a e-mail.</div>
              <div class="alert alert-danger error" id="subjectError"><i class="icon-cancel"></i> Oh snap! Subject field can't stay empty.</div>
            </div>
            <div class="col-lg-12">
              <p class="text_cont"><textarea name="message" placeholder="Message" id="message" class="input-cont-textarea" cols="40" rows="10"></textarea></p>
              <div class="alert alert-danger error" id="messageError"><i class="icon-cancel"></i> Oh snap! This field can't stay empty.</div>
              <div class="alert alert-success success" id="success"><i class="icon-ok"></i> Well done! You message is successfully sent.</div>
            </div>
            <div class="col-lg-12"><p><input type="submit" id="send" class="btn btn-default" value="Send message" /></p></div>
          </form>
        </div>
      </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12">
      <div class="promo-block">
        <div class="promo-text">Information</div>
        <div class="center-line"></div>
      </div>
      <div class="marg50">
        <ul class="contact-composer">
          <li><i class="pe-7s-home"></i> <span>Address: {{ config('settings.address') }}</span></li>
          <li><i class="pe-7s-call"></i> <span>Phone: {{ config('settings.phone') }}</span></li>
          <li><i class="pe-7s-print"></i> <span>Fax: {{ config('settings.fax') }}</span></li>
          <li><i class="pe-7s-mail"></i> <span>E-mail: {{ config('settings.email') }}</span></li>
        </ul>
      </div>
    </div>
  </div>
</div>
