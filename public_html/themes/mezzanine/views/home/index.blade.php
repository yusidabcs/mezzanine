 <div id="rev_slider_68_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="web-product-dark-hero64" style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;height:850px;">
  <div id="rev_slider_68_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
    <ul>  
      @foreach(get_main_gallery() as  $key => $g)
      <li data-index="rs-20" data-transition="zoomin" data-slotamount="7"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-rotate="0"  data-saveperformance="off"  data-title="Love it?" data-description="">
        <img src="{{ url($g->image) }}"  alt=""  data-bgposition="center center" data-kenburns="on" data-duration="00000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="100" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 -500" data-offsetend="0 500" data-bgparallax="10" class="rev-slidebg" data-no-retina>



        <div class="tp-caption NotGeneric-SubTitle   tp-resizeme rs-parallaxlevel-0" 
        id="slide-20-layer-4" 
        data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
        data-y="['middle','middle','middle','middle']" data-voffset="['52','52','52','51']" 
        data-width="none"
        data-height="none"
        data-whitespace="nowrap"
        data-transform_idle="o:1;"

        data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
        data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
        data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
        data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
        data-start="1250" 
        data-splitin="none" 
        data-splitout="none" 
        data-responsive_offset="on" 


        style="z-index: 6; white-space: nowrap;">{!! $g->description !!} 
      </div>
    </li>
    @endforeach

  </ul>
  <div class="tp-static-layers">
  </div>
  <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div> 
</div>
</div><!-- END REVOLUTION SLIDER -->
<script type="text/javascript">
  var tpj=jQuery;           
  var revapi68;
  tpj(document).ready(function() {
    if(tpj("#rev_slider_68_1").revolution == undefined){
      revslider_showDoubleJqueryError("#rev_slider_68_1");
    }else{
      revapi68 = tpj("#rev_slider_68_1").show().revolution({
        sliderType:"standard",
        jsFileLocation: "assets/rs-plugin/js/",
        sliderLayout:"fullwidth",
        dottedOverlay:"none",
        delay:9000,
        navigation: {
          keyboardNavigation:"off",
          keyboard_direction: "horizontal",
          mouseScrollNavigation:"off",
          onHoverStop:"off",
          touch:{
            touchenabled:"on",
            swipe_threshold: 75,
            swipe_min_touches: 1,
            swipe_direction: "horizontal",
            drag_block_vertical: false
          }
          ,
          arrows: {
            style: "uranus",
            enable: true,
            hide_onmobile: true,
            hide_under: 778,
            hide_onleave: true,
            hide_delay: 200,
            hide_delay_mobile: 1200,
            tmp: '',
            left: {
              h_align: "left",
              v_align: "center",
              h_offset: 20,
              v_offset: 0
            },
            right: {
              h_align: "right",
              v_align: "center",
              h_offset: 20,
              v_offset: 0
            }
          }
        },
        responsiveLevels:[1240,1024,778,480],
        gridwidth:[1400,1240,778,480],
        gridheight:[850,850,960,720],
        lazyType:"none",
        parallax: {
          type:"mouse+scroll",
          origo:"slidercenter",
          speed:2000,
          levels:[1,2,3,20,25,30,35,40,45,50],
          disable_onmobile:"on"
        },
        shadow:0,
        spinner:"off",
        autoHeight:"off",
        disableProgressBar:"on",
        hideThumbsOnMobile:"off",
        hideSliderAtLimit:0,
        hideCaptionAtLimit:0,
        hideAllCaptionAtLilmit:0,
        debugMode:false,
        fallbacks: {
          simplifyAll:"off",
          disableFocusListener:false,
        }
      });
    }
  }); /*ready*/
</script>
<div class="container-color">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="promo-block">
          <div class="promo-text">Mezzanine Member</div>
          <div class="promo-paragraph">Attended no do thoughts me on dissuade scarcely</div>
          <div class="center-line"></div>
        </div>
      </div>
    </div>
    <div class="row marg30">
      @foreach(get_members(4) as $member)
      
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="team-image">
          <a href="{{ author_url($member) }}" ><img src="{{ $member->imageURL() }}" class="" alt=""></a>
        </div>
        <div class="team-block">
          <div class="about-name">{{ $member->name }}</div>
          <ul class="soc-about text-center">
            <li><a href="{{ $member->facebook }}"><i class="ti-facebook"></i></a></li>
            <li><a href="{{ $member->twitter }}"><i class="ti-twitter"></i></a></li>
            <li><a href="{{ $member->instagram }}"><i class="ti-instagram"></i></a></li>
          </ul>
        </div>
      </div>
      @endforeach
    </div>
  </div>
</div>

    <div class="marg75">

        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <div class="promo-block">
                <div class="promo-text">Our Gallery</div>
                <div class="promo-paragraph">Attended no do thoughts me on dissuade scarcely</div>
                <div class="center-line"></div>
              </div>
            </div>
          </div>
        </div>
        
        <div class="grid gallery js-isotope marg30">
          @foreach(get_gallery() as  $key => $g)
          <div class="grid-item">
            <div class="portfolio-dankov">
               <figure class="effect-goliath">
                <img src="{{ $g->image }}" alt="img23"/>
                <figcaption>
                  <div class="icon-links">
                    <a href="{{ $g->url }}" class="cbp-singlePageInline attach-icon"><i class="pe-7s-link"></i></a>
                    <a href="{{ $g->image }}" class="cbp-lightbox search-icon" data-gal="prettyphoto"><i class="pe-7s-search"></i></a>
                  </div>
                  {!! $g->description !!}
                </figcaption>     
              </figure>
            </div>
          </div>
          @endforeach
          
            
        </div>
      </div>

      <div class="container-color marg75">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <div class="promo-block">
                <div class="promo-text">Recent Posts</div>
                <div class="promo-paragraph">Attended no do thoughts me on dissuade scarcely</div>
                <div class="center-line"></div>
              </div>
            </div>
          </div>
          <div class="row marg30">
            @foreach(get_news(3) as $news)
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-ms-12">
              <div class="blog-main">
                @if($news->image)
                <div class="blog-images">
                    <div class="post-thumbnail">
                      <div class="single-item"></div>
                      <div class="single-action">
                        <span><a href="{{ $news->url() }}"><i class="pe-7s-pen"></i></a></span>
                      </div>
                      <img src="assets/images/blog1.jpg" alt="You fully seems stand inquietude own">
                    </div>
                </div>
                @endif
                <div class="blog-name"><a href="{{ $news->url() }}">{{ $news->title }}</a></div>
                <div class="blog-text"><p>{{ $news->shortDescription() }}</p></div>
                <div class="blog-desc">
                  <ul>
                    <li><i class="pe-7s-date"></i> {{ date('d M, Y',strtotime($news->created_at)) }} </li>
                    <li><i class="pe-7s-user"></i> <a href="#">{{ $news->author->name }}</a></li>
                  </ul>
                </div>              
              </div>
            </div>
            @endforeach
          </div>
        </div>
      </div>