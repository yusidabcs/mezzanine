<div class="page-in " style="background: url('{{ $page->image  }}');">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
      <div class="page-in-name">{{ $page->title }}</div>
        <div class="page-in-bread">You are here: <a href="#">Home</a> \ {{ $page->title }}</div>              
      </div>            
    </div>
  </div>
</div>
<div class="container marg75">
  <div class="row">
    <div class="col-lg-12 marg30">
      {!! $page->content !!}
    </div>
  </div>
</div>