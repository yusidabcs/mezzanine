@extends('auth.layout')

@section('content')
    
<div class="login-box">
  <div class="login-logo">
    <a href="../../index2.html"><b>Admin</b>LTE</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>
    <form role="form" method="POST" action="{{ url('admin/login') }}">
      {!! csrf_field() !!}
      <div class="form-group has-feedback">
        <input type="email" class="form-control" name="email" value="{{ old('email') }}">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>

        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>

        @if ($errors->has('password'))
        <?php dd($errors);?>
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif


      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox" name="remember"> {{ trans('backpack::base.remember_me') }}
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary">
                {{ trans('backpack::base.login') }}
            </button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <hr>

    <a style="float: left;" href="{{ url('admin/password/reset') }}">{{ trans('backpack::base.forgot_your_password') }}</a>
    <a style="float: right;" href="register.html" class="text-center">Register a new membership</a>
    <div style="clear: both;"></div>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

@endsection