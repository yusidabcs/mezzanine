<!-- text input -->

<?php

// the field should work whether or not Laravel attribute casting is used
if (isset($field['value']) && (is_array($field['value']) || is_object($field['value']))) {
    $field['value'] = json_encode($field['value']);
}

?>
<!-- field_type_name -->
<div @include('crud::inc.field_wrapper_attributes') >
    <label>{!! $field['label'] !!}</label>
    <input
        type="hidden"
        data-location="{{ old($field['name']) ? old($field['name']) : (isset($field['value']) ? $field['value'] : (isset($field['default']) ? $field['default'] : '' )) }}"
        name="{{ $field['name'] }}"
        value="{{ old($field['name']) ? old($field['name']) : (isset($field['value']) ? $field['value'] : (isset($field['default']) ? $field['default'] : '' )) }}"
        @include('crud::inc.field_attributes')
    >
<div id="map"></div>


    {{-- HINT --}}
    @if (isset($field['hint']))
        <p class="help-block">{!! $field['hint'] !!}</p>
    @endif
</div>


@if ($crud->checkIfFieldIsFirstOfItsType($field, $fields))

	{{-- FIELD CSS - will be loaded in the after_styles section --}}
	@push('crud_fields_styles')
		 <style>
			/* Always set the map height explicitly to define the size of the div
			* element that contains the map. */
			#map {
			height: 200px;
			width: 100%;
			}
			</style>
	@endpush

	@push('crud_fields_scripts')
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB54_VU_DpSZgnlBMjC57hTO8VLG22a-w0&callback=initMap"
    async defer></script>
	@endpush

@endif

{{-- FIELD JS - will be loaded in the after_scripts section --}}
@push('crud_fields_scripts')
	<script>
      var map;
      function initMap() {
      
      var myLatLng = JSON.parse($('[data-location]').val());

	  var map = new google.maps.Map(document.getElementById('map'), {
	    zoom: 10,
	    center: myLatLng
	  });

	  var marker = new google.maps.Marker({
	    position: myLatLng,
	    map: map,
	    title: 'Drag me to your location!',
	    draggable:true,
	  });

	  marker.addListener('dragend',function(){
	  	var obj = {
	  		'lng': marker.getPosition().lng(),
	  		'lat': marker.getPosition().lat()
	  	};

	    $('[data-location]').each(function(){

            $(this).val(JSON.stringify(obj));
        });
	  	
	  });

      }
    </script>
    
@endpush
